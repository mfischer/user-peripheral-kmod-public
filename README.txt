This is a thin layer kernel module that allows userland applications
such as GNU Radio to talk to the FPGA fabric in a Zynq EPP, by mmap-ing
stuff to userland.

Questions to: <moritz dot fischer at ettus dot com>
